package main

import (
	"log"
	"os"
	"path/filepath"
)

func main() {
	wd, err := os.Getwd()
	whenErrThenExit(err)

	dir, err := os.Open(wd)
	whenErrThenExit(err)

	items, err := dir.Readdirnames(0)
	whenErrThenExit(err)

	for _, v := range items {

		switch filepath.Ext(v) {
		case ".txt", ".md":
			os.Rename(v, filepath.Join("text", v))
		case ".go", ".dart", ".sh":
			os.Rename(v, filepath.Join("code", v))
		case ".jpg", "png":
			os.Rename(v, filepath.Join("image", v))
		case ".mp4", "flv":
			os.Rename(v, filepath.Join("video", v))
		case ".azw3", ".mobi", ".epub", ".pdf":
			os.Rename(v, filepath.Join("ebook", v))
		case ".deb", ".exe", ".zip":
			os.Rename(v, filepath.Join("archive", v))

		}

	}

}

func whenErrThenExit(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
